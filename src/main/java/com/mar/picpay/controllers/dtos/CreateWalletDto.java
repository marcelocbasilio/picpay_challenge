package com.mar.picpay.controllers.dtos;

import com.mar.picpay.entities.Wallet;
import com.mar.picpay.entities.WalletType;

public record CreateWalletDto(String fullName, String cpfCnpj, String email, String password,
		WalletType.Enum walletType) {

	public Wallet toWallet() {
		return new Wallet(fullName, cpfCnpj, email, password, walletType.get());
	}

}
