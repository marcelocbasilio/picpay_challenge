package com.mar.picpay.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mar.picpay.entities.WalletType;

public interface WalletTypeRepository extends JpaRepository<WalletType, Long> {

}
