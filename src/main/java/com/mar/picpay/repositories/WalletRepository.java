package com.mar.picpay.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mar.picpay.entities.Wallet;

public interface WalletRepository extends JpaRepository<Wallet, Long> {

	Optional<Wallet> findByCpfCnpjOrEmail(String cpfCnpj, String email);

}
