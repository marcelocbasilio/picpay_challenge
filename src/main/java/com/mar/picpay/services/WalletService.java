package com.mar.picpay.services;

import org.springframework.stereotype.Service;

import com.mar.picpay.controllers.dtos.CreateWalletDto;
import com.mar.picpay.entities.Wallet;
import com.mar.picpay.exception.WalletDataAlreadyExistsException;
import com.mar.picpay.repositories.WalletRepository;

@Service
public class WalletService {

	private final WalletRepository walletRepository;

	public WalletService(WalletRepository walletRepository) {
		this.walletRepository = walletRepository;
	}

	public Wallet createWallet(CreateWalletDto dto) {
		var walletDb = walletRepository.findByCpfCnpjOrEmail(dto.cpfCnpj(), dto.email());
		if (walletDb.isPresent()) {
			throw new WalletDataAlreadyExistsException("CPF/CNPJ or E-mail already exists.");
		}
		return walletRepository.save(dto.toWallet());
	}

}
